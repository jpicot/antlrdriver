# antlrdriver

Antlrdriver is a simple Python module to run ANTLR4-generated code.

## Installation

As a regular python module, fetch the archive file in the `dist/` folder, and

    pip install --user antlrdriver-$VERSION.tar.gz

If you want to develop, clone the git repository, and:

    pip install -e <path-to-cloned-repo>

Note: the use of `setup.py` is deprecated[^1].

## Usage

The command-line program is accessible either via `python -m antlrdriver` or
via the script `antlrdriver` stored in `$HOME/.local/bin`. As argument, you
should provide at least a lexer, then a parser, and as many listener or visitor
you want.

    antlrdriver lexer.py parser.py listener_or_visitor.py…

Source must be provided from stdin. You can also use an interactive
interpreter:

    python
    » import antlrdriver
    » antlrdriver.process("lexer_mod", "parser_mod", ["listener_or_visitor_mods"], "data_string")

where `"lexer_mod"` and `"parser_mod"` are either `None` or the name of the
Lexer/Parser module, these module should be located in the current working
directory, `["listener_or_visitor_mods"]` is a list of listener and visitor
modules, and `"data_string"` is the data to test.

[^1]: https://blog.ganssle.io/articles/2021/10/setup-py-deprecated.html#id4
