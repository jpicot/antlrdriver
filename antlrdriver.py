import importlib
import logging
import os
import sys

import antlr4


log_handler = logging.StreamHandler()
log_handler.setFormatter(logging.Formatter("[%(name)s] %(message)s"))

log = logging.getLogger("driver")
log.addHandler(log_handler)

lexer_log = logging.getLogger("lexer")
lexer_log.addHandler(log_handler)

log.setLevel(logging.INFO)
lexer_log.setLevel(logging.INFO) ## Set to WARNING to hide lexer messages.


def processor(lexer_arg, parser_arg, application_args):
    """
    Generates a function that process its argument.

    """
    def func(source):
        if lexer_arg is None:
            log.error("please, provide at least a lexer")
            return (None, None, None)

        ## Add curent directory to module search path.
        sys.path.insert(0, os.curdir)

        Lexer = load_module_object(lexer_arg)
        input_stream = antlr4.InputStream(source)
        stream = antlr4.CommonTokenStream(Lexer(input_stream))

        log.info("lexing…")
     
        stream.fill()
        #lexer_log = logging.getLogger("driver.lexer")
        for tok in stream.tokens:
            lexer_log.info("%s %s", tok, get_token_type(tok, Lexer))

        if parser_arg is None:
            log.info("finished (no parser).")
            return (stream, None, None)

        log.info("parsing…")

        Parser = load_module_object(parser_arg)
        parser = Parser(stream)
        tree = getattr(parser, parser.ruleNames[0])()

        if application_args is None:
            log.info("finished (no listener nor visitor).")
            return (stream, parser, None)

        applications = list()

        for application_arg in application_args:

            App = load_module_object(application_arg)

            if not issubclass(App, (antlr4.ParseTreeListener,
                                    antlr4.ParseTreeVisitor)):
                log.warning("what is provided is neither a Listener nor a Visitor")
                applications.append(None)
                continue

            if issubclass(App, antlr4.ParseTreeListener):

                log.info("listening (%s)…", App.__name__)

                walker = antlr4.ParseTreeWalker()
                app = App()
                walker.walk(app, tree)

            else:

                log.info("visiting (%s)…", App.__name__)

                app = App()
                app.visit(tree)

            applications.append(app)


        log.info("Finished")

        return (stream, parser, applications)

    return func


def load_module_object(path):
    name = os.path.splitext(path)[0]
    mod = importlib.import_module(name)
    return getattr(mod, name)


def get_token_type(token, Lexer):
    try:
        return Lexer.symbolicNames[token.type]
    except IndexError:
        return "unknown"


def main():

    ## Interpret command-line.

    if len(sys.argv) <= 1 or {"-h", "--help"} & set(sys.argv):
        log.error("usage: prog lexer.py [parser.py [listener.py | visiter.py]]")
        return 2

    lexer, parser, applications = None, None, []

    if len(sys.argv) > 1: lexer = sys.argv[1]
    if len(sys.argv) > 2: parser = sys.argv[2]
    if len(sys.argv) > 3: applications = sys.argv[3:]

    ## Lexing, parsing, listening.

    try:
        process = processor(lexer, parser, applications)
        process(sys.stdin.read())

    except RuntimeError:
        return 1
    
    return 0


if __name__ == "__main__":

    sys.exit(main())
